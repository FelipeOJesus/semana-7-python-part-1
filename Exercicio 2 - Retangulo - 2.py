largura = int(input("digite a largura: "))
altura = int(input("digite a altura: "))

alturaInicial = altura

while (altura > 0):
    coluna = 1
    
    if(alturaInicial == altura or altura == 1):
        while(coluna <= largura):
            print("#", end="")
            coluna+=1
    else:
        while(coluna <= largura):
            if(coluna==1 or coluna == largura):
                print("#", end="")
            else:
                print(" ", end="")
            coluna+=1

    print()
    altura -=1;
