def n_primos (x):
    quantidadeDePrimos = 0
    while( x >= 2 ):
        contador = 0;
        i = 1
        while( i <= x):
            if(x%i == 0):
                contador += 1;
            i +=1
        if(contador <= 2):
            quantidadeDePrimos += 1
        x-=1
    return quantidadeDePrimos

def main():
    print(n_primos(2))
    print(n_primos(4))
    print(n_primos(121))

main()